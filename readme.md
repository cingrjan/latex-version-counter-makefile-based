This is really simple counter that will increment number in a 
file each time you use included `makefile` for compilation of 
your LaTeX document. The number can by included in your document 
by simply including contents of the `count` file (for examle 
by using `\input{count}` function).

## How to use this

You have to copy following files into your project
- `clear.py`
- `counter.py`
- `makefile`

After that you have to change the name of your file in the `makefile`.
For example your TeX file is called `document1.tex` so you have to open the `makefile`
and chagne the `filename` variable to your file name.
```makefile
# FILE NAME OF THE PROJECT
filename := document1
```

Next you have to create the file with variable number, that 
will be incremented each time you compile your TeX document.
You can do tahat by following commad
```bash
make init
```

To include the version number you just have to inmput content of
the `count` file. You can do taht by `\input` LaTeX command.
```latex
\input{count}
```

Now you shloud be able to use the `makefile` to compile 
your work. You can do taht by command
```bash
make
```

## Makefile functions

- `make all`
    will increment the number in `count` file and then it will
    compile your TeX document

- `no-version`
    will compile your TeX document withouth incrementing the number

- `increment`
	will increment the number withouth compilation

- `open`
	this will open pdf in Firefox

- `clear-version` 
    this will write out new file called `count` with
    0 in it (if there is existing `count` file it will be replaced)

- `init` 
	same as `clear-version`
