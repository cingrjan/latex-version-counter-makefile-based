# FILE NAME OF THE PROJECT
filename := test

all:
	python counter.py
	pdflatex --shell-escape $(filename).tex

no-version:
	pdflatex --shell-escape $(filename).tex

increment:
	python counter.py

open:
	firefox $(filename).pdf

clear-version:
	python clear.py

init:
	python clear.py
